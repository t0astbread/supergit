# supergit
A git branching model that prioritizes simplicity, semantics and
a clean history


## Branching
Branches have a __type__ and a __name__.

The syntax for branch naming (in git) looks like this:  
`<type>/<name>`

### Branch types

#### Topic branches
Topic branches are where most of your work happens. Typically only
one person will work on a topic branch (however, this is not a
hard rule).

| Type   | Name                | Description                                                     |
|--------|---------------------|-----------------------------------------------------------------|
| `feat` | Feature branch      | New features or changes                                         |
| `exp`  | Experimental branch | Experiments on the codebase; can be messy, should not be merged |
| `fix`  | Fix branch          | Bug fixes                                                       |


#### Master-like branches
Master-like branches are branches for special releases. Usually
releases are just tags on master (the actual `master`), however
maintining more than one product line or other special cases might
require the use of master-like branches.

A master-like can also be referred to as a __m*ster__.

| Type   | Name              | Description                               |
|--------|-------------------|-------------------------------------------|
| `flav` | Flavor branch     | Master for a different product flavor     |
| `pick` | Cherrypick branch | Cherrypick release for a special occasion |


#### Volatile branches
These branches typically only exist for a short amount of time.
They are never merged directly to master (or a master-like).

| Type    | Name                | Description                                                          |
|---------|---------------------|----------------------------------------------------------------------|
| `push`  | Push branch         | Extremely messy; just to have your work saved somewhere in the cloud |
| `trans` | Transmission branch | Branch to transmit your changes between workstations                 |


### Commits

#### Commit messages
Commit messages are generally written in __present tense__. They
don't represent what has been done, but rather they represent the
author's __intention__ in that commit. Commits that don't fulfill
their promised intention are amended before merge.

_Layout of a commit message:_
```
Title               - max. 50 characters

Message paragraph   - max. 70 characters per line

Message paragraph
...
```

#### Scope of a single commit
Each commit should encapsulate one complete seperate logical change.
Avoid "cowboy commits".

Good heuristics to detect what should be a seperate commit:
  - Is this a change that you would want to reverse to?
  - Is this a change that you would want to cherrypick?
  - Do I have to use an "and" in the commit message?
  - Might I want to use `git bisect` to detect a bug in this snippet
    in the future? (In other words: "Does this code have bugs?" -
    Which is always yes.)

#### Commit completeness
Different branch types have different requirements regarding the
completeness of their commits.

| Branch type   | Must build | Must be one logical change | Must pass tests | Merged into |
|---------------|------------|----------------------------|-----------------|-------------|
| Master-like   | __YES__    |          __YES__           |     __YES__     | -           |
| `feat`, `fix` | __YES__    |          __YES__           |     __YES__     | Master-like |
| `exp`         | __YES__    |            NO              |       NO        | -           |
| Volatile      | NO         |            NO              |       NO        | Topic       |

_On experimental branches:_  
Experimental branches don't need tests. They can also break existing
tests. Experimental branches can contain multiple changes in one
commit (for when you just want to try something out). They should,
however, never be merged. __Re-write experiments with tests and a clean
history__ on a feature- or fix branch before you merge them.


### Testing
Commits that finish a testable change should be immediately followed
by a commit (or multiple commits) that introduce relevant tests for
the change. This way, tools like `git bisect` will produce more
accurate results.

Ideally, every commit should be tested before its branch is merged,
as to not introduce any pollution in the target branch that is not
caught by just testing the latest commit in the source branch.

Testing always means testing the whole project (unless specified
otherwise). For faster development you can of course run a subset of
your whole test suite but before critical events like merges or
releases, always run every test in the affected project(s).


## Merging
Merging should be done in a way that makes it impossible for errors
to be introduced during the merge.

This means that non-fast-forward merges are forbidden. Our source
branch needs to be a direct extension of our target branch when
merging (resulting in a fast-forward).

To accomplish this, the source branch should (if it's not a direct
extension already) be rebased to the target branch. See the
illustration below.

![Comparison of a rebase workflow to a non-ff merge workflow.
The non-ff workflow eventually results in a broken history while the
rebase workflow results in a clean, comprehensable history.
](rebase_vs_merge.svg)
